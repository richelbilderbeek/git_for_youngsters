#!/bin/bash

# Concatenate all Markdown files first, convert those to one PDF

build_folder=build
#echo $build_folder

rm -rf build
mkdir build

if [ ! -d $build_folder ]; then 
  echo "Error: failed to create build folder"
  exit 1
fi


cp ../chapters/foreword/*.*              $build_folder ; mv $build_folder/README.md $build_folder/README_00.md
cp ../chapters/setup_git/*.*             $build_folder ; mv $build_folder/README.md $build_folder/README_01.md
cp ../chapters/register/*.*              $build_folder ; mv $build_folder/README.md $build_folder/README_02.md
cp ../chapters/install_discord/*.*       $build_folder ; mv $build_folder/README.md $build_folder/README_03.md
cp ../chapters/create_repo/*.*           $build_folder ; mv $build_folder/README.md $build_folder/README_04.md
cp ../chapters/monologue/*.*             $build_folder ; mv $build_folder/README.md $build_folder/README_05.md
cp ../chapters/fairytale_on_master/*.*   $build_folder ; mv $build_folder/README.md $build_folder/README_06.md
cp ../chapters/hello_world_on_master/*.* $build_folder ; mv $build_folder/README.md $build_folder/README_07.md


cd $build_folder

cat README_00.md >> README.md; echo " " >> README.md; echo "\pagebreak" >> README.md; echo " " >> README.md
cat README_01.md >> README.md; echo " " >> README.md; echo "\pagebreak" >> README.md; echo " " >> README.md
cat README_02.md >> README.md; echo " " >> README.md; echo "\pagebreak" >> README.md; echo " " >> README.md
cat README_03.md >> README.md; echo " " >> README.md; echo "\pagebreak" >> README.md; echo " " >> README.md
cat README_04.md >> README.md; echo " " >> README.md; echo "\pagebreak" >> README.md; echo " " >> README.md
cat README_05.md >> README.md; echo " " >> README.md; echo "\pagebreak" >> README.md; echo " " >> README.md
cat README_06.md >> README.md; echo " " >> README.md; echo "\pagebreak" >> README.md; echo " " >> README.md
cat README_07.md >> README.md; echo " " >> README.md; echo "\pagebreak" >> README.md; echo " " >> README.md

pandoc README.md -o book.pdf --toc --toc-depth=1 --highlight-style=tango -V geometry:margin=0.5in
cp book.pdf ../../books/book_1_without_front_page.pdf

cd ../../books
pdfunite front_page_book_1.pdf book_1_without_front_page.pdf book_1.pdf

# Make booklet
bookletimposer -a book_1.pdf -o booklet_1.pdf

rm book_1_without_front_page.pdf
