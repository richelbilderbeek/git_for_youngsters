#!/bin/bash
#
# Add borders to add images of a chapter
#
# Usage:
#
# In a chapter's folder, do:
#
#   ../../scripts/add_borders.sh
#

for input_filename in $(ls *.png | grep -E -v "_with_border|emoji_")
do
  echo "input_filename: ${input_filename}"
  output_filename=${input_filename%.*}_with_border.png
  echo "output_filename: ${output_filename}"
  ../../scripts/add_border.sh "${input_filename}" "${output_filename}"
done
