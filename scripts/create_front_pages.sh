#!/bin/bash

# Concatenate all Markdown files first, convert those to one PDF

build_folder=build

rm -rf build
mkdir build

if [ ! -d $build_folder ]; then 
  echo "Error: failed to create build folder"
  exit 1
fi

cp ../chapters/front_pages/*.* $build_folder
cd $build_folder

pandoc front_page_book_1.md -o ../../books/front_page_book_1.pdf

