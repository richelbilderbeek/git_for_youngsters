#!/bin/bash

echo "Parameters: $@"
echo "Number of parameters: $#"

if [[ "$#" -ne 2 ]] ; then
  echo "Invalid number of arguments: must have 2 parameters: "
  echo " "
  echo "  1. input_filename"
  echo "  2. output_filename"
  echo " "
  echo "Actual number of parameters: $#"
  echo " "
  echo "Exiting :-("
  exit 42
fi

echo "Correct number of arguments: $#"
input_filename=$1
output_filename=$2
echo "input_filename: ${input_filename}"
echo "output_filename: ${output_filename}"

if [ "${input_filename}" == "${output_filename}" ]
then
  echo "Output file must differ from the input file, to prevent overwriting"
  echo "input_filename: ${input_filename}"
  echo "output_filename: ${output_filename}"
  exit 42
fi

convert "${input_filename}" -border 2x2 "${output_filename}"
