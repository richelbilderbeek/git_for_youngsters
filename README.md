# git_for_youngsters

Exercises to learn git and Codeberg for 8-18 year olds.

 * [Chapters](./chapters/README.md)
 * [Foreword](./chapters/foreword/README.md)
 * [Lesson card](./chapters/lesson_card/README.md)

## Discord

We work online using Discord.

Send a friend request to `richelbilderbeek#9002` and he will invite you
to our Discord server.

## Links

 * [Programmeringskurs foer ungdomar](https://github.com/richelbilderbeek/programmeringskurs_foer_ungdomar):
   programming course for youngsters

