# Lesson card

 * Lesson card: [PDF](lesson_card.pdf) [ODT](lesson_card.odt)

## Logos

From [https://git-scm.com/downloads/logos](https://git-scm.com/downloads/logos).

![](Git-Icon-1788C.png)

![](Git-Logo-2Color.png)

