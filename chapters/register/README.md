# 2. Register and sign in

In this chapter, we will register and sign in to Codeberg.

![](emoji_bowtie.png) | Codeberg hosts git repositories for free
:-------------:|:----------------------------------------: 

\pagebreak

## 2.1 Exercise 1

To register, go to [https://codeberg.org/](https://codeberg.org/) 
and click on 'Register'.

![](register_with_border.png)

Do so.

![](emoji_sunglasses.png) | Of course, Codeberg needs to know how cool I am
:-------------:|:----------------------------------------: 

\pagebreak

## 2.2 Exercise 2

Go to [https://codeberg.org/](https://codeberg.org/) and click on 'Register'
to sign in:

![](sign_in_with_border.png)

Do so, you will be taken to the Dashboard.

![](emoji_sunglasses.png) | Of couse, Codeberg is happy to let me in
:-------------:|:----------------------------------------: 

\pagebreak

## 2.3 Final exercise

Register and sign in. This will take you to the Dashboard.

![](dashboard_with_border.png)

