# 6. Fairytale on `master`

In this chapter, we are going to use the basic `git` workflow
by writing a fairytale with a team.

![](emoji_bowtie.png) | A fairytale is a story in which anything can happen
:-------------:|:----------------------------------------: 

## 6.1 Exercise 1

On Codeberg, create a new repository.

![](emoji_sunglasses.png) | Easy peasy!
:-------------:|:----------------------------------------: 

\pagebreak

## 6.2 Adding Collaborators

![](emoji_bowtie.png) | A Collaborator is someone in your team
:-------------:|:----------------------------------------: 

To add Collaborators, in your repo, click on 'Settings' (at the top right),
then click on the 'Collaborators' tab.

Add one or more Collaborators. Collaborators are people you work together
with. They too can now modify the text.

## 6.3 Exercise 2

Add at least 1 Collaborator.

![](emoji_sunglasses.png) | Add me, I am the best!
:-------------:|:----------------------------------------: 

\pagebreak

## 6.4 Basic workflow

![](emoji_bowtie.png) | You already know this
:-------------:|:----------------------------------------: 

Clone the repository on your computer:

```
git clone [repo_url]
```

For example,

```
git clone https://codeberg.org/richelbilderbeek/iloverichel
```

Navigate into the folder of that repository:

```
cd [repo_name]
```

For example,

```
cd iloverichel
```

Modify the `README.md`, for example

```
notepad README.md
```

![](emoji_bowtie.png) | Fairytales usually start with 'Once upon a time'
:-------------:|:----------------------------------------: 


![](emoji_smiley.png) | Fairytales usually end with 'And they lived happily ever after'
:-------------:|:----------------------------------------: 


Do `add`, `commit`, `push`:

```
git add .
git commit -m "Improve"
git push
```

## 6.5 Exercise 3

Each team member must 

 * add a sentence to the fairytale
 * `add`, `commit` and `push` it

If something goes wrong, go to the next paragraph.

![](emoji_smiley.png) | The story is unimportant here
:-------------:|:----------------------------------------: 

\pagebreak

## 6.6 `git pull`

If at `git push` you get a long text similar to below, 
you need to update your local repository, see below that :-)

```
richel@N141CU:~/codeberg/git_for_youngsters$ git push
To https://codeberg.org/richelbilderbeek/git_for_youngsters.git
 ! [rejected]        master -> master (fetch first)
error: failed to push some refs to 'https://codeberg.org/richelbilderbeek/git_for_youngsters.git'
hint: Updates were rejected because the remote contains work that you do
hint: not have locally. This is usually caused by another repository pushing
hint: to the same ref. You may want to first integrate the remote changes
hint: (e.g., 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

![](emoji_bowtie.png) | If you read the text, you can read you should do a `git pull`
:-------------:|:----------------------------------------: 

The reason for this text is,
when you work together, it can be that the version on your computer is
outdated: a Collaborator can have `push`ed a new version as well.
To update, do:

```
git pull
```

![](emoji_bowtie.png) | Tip: you can never do `git pull` too often
:-------------:|:----------------------------------------: 

![](emoji_bowtie.png) | Tip: when in doubt, do `git pull`
:-------------:|:----------------------------------------: 

\pagebreak

## 6.7 Exercise 4

At the same time, each team member must 

 * add a sentence to the fairytale
 * `add`, `commit` and `push` it
 * get the message that suggests to do `git pull`
 * do a `git pull`

If you get an error message like below, move on to the next paragraph.

```
CONFLICT (content): Merge conflict in README.md
```

![](emoji_computer.png) | If your team works smart, there will be few merge conflicts
:-------------:|:----------------------------------------: 

\pagebreak

## 6.8 Merge conflict

When two people work on different files, or on different parts of the same
file, `git` can easily combine the changes. However, when `git` is not
sure what to do, it will give you a merge conflict.

For example, assume `README.md` is:

```
To be
or not to be
```

One person changes this to:

```
To be
a dinosaur
or not to be
```

The second person, however, changes his/her version of `README.md` to:

```
To be a crocodile
or not to be
```

When the first person pushes his/her work, there is no problem.
A problem will arise when the second person tries to push (which fails),
then pulls: a merge conflict. This looks similar to this:

```
richel@N141CU:~/codeberg/git_for_youngsters$ git pull
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 3 (delta 2), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), 294 bytes | 294.00 KiB/s, done.
From https://codeberg.org/richelbilderbeek/git_for_youngsters
   7879d89..5e32784  master     -> origin/master
Auto-merging README.md
CONFLICT (content): Merge conflict in README.md
Automatic merge failed; fix conflicts and then commit the result.
```

The second person, with the merge conflict, needs to solve this.
This can be done by editing `README.md`.
It will look similar to:

```
<<<<<<<<<<<<
To be
a dinosaur
============
To be a crocodile
>>>>>>>>>>>>
or not to be
```

The `<<`s and `==`s and `>>`s are nothing special: they are just text
and can be deleted like any other text.

The second person now needs to **think** and change the text to whatever
is best, for example, to:

```
To be a crocodile or dinosaur 
or not to be
```

This is a change, hence a `add`, `commit` and `push` needs to be done.

## 6.9 Exercise 5

Work together on a fairytale, with 5 commits per person.
Also, each person must have solved one merge conflict.
If you get no merge conflicts, **cause** them.

![](emoji_bowtie.png) | We work on a fairytale, as that 'always works' (unlike code)
:-------------:|:----------------------------------------: 

