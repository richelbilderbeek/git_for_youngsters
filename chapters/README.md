# Chapters

Chp|Description
---|---------------------------------------------------------------
.  |**Alone**
1  |[Setup git](./setup_git/README.md)
2  |[Register and sign in](./register/README.md)
3  |[Install Discord](./install_discord/README.md)
.  |**Newbie team**
4  |[Create a repository](./create_repo/README.md)
5  |[Monologue](./monologue/README.md)
6  |[Fairytale on `master`](./fairytale_on_master/README.md)
7  |['Hello world' on `master`](./hello_world_on_master/README.md)
.  |**Beginner team**
8  |:construction: [Fairytale on branches](./fairytale_on_branches/README.md)
9  |:construction: ['Hello world' on branches](./hello_world_on_branches/README.md)
10 |:construction: ['Tron' and Issues](./tron_and_issues/README.md)
.  |**Advanced team**
11 |:construction: [Fairytale with code reviews](./fairytale_with_code_review/README.md)
12 |:construction: ['Hello world' with code reviews](./hello_world_with_code_review/README.md)
13 |:construction: ['Tron' and Project](./tron_and_project/README.md)

Other folders:

 * [Foreword](./foreword/README.md)
 * [Lesson card](./lesson_card/README.md)

