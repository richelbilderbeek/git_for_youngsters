# 7. 'Hello World' on `master`

In this chapter, we are going to use the basic `git` workflow
by writing a simple program with a team.


![](emoji_bowtie.png) | A fairytale is a story in which anything can happen
:-------------:|:----------------------------------------: 

## 7.1 Exercise 1

On Codeberg, create a new repository.

![](emoji_sunglasses.png) | Easy peasy!
:-------------:|:----------------------------------------: 

\pagebreak

## 7.2 Adding Collaborators

![](emoji_bowtie.png) | A Collaborator is someone in your team
:-------------:|:----------------------------------------: 

To add Collaborators, in your repo, click on 'Settings' (at the top right),
then click on the 'Collaborators' tab.

Add one or more Collaborators. Collaborators are people you work together
with. They too can now modify the code.

## 7.3 Exercise 2

Add at least 1 Collaborator.

![](emoji_sunglasses.png) | Add me, I am the best!
:-------------:|:----------------------------------------: 

\pagebreak

## 7.4 Clone the repo

![](emoji_bowtie.png) | You already know this
:-------------:|:----------------------------------------: 

Clone the repository on your computer:

```
git clone [repo_url]
```

For example,

```
git clone https://codeberg.org/richelbilderbeek/iloverichel
```

## 7.5 Exercise 3

Clone your repo

\pagebreak

## 7.6 Processing

Start Processing.

Below is a short Processing program:

```c++
void draw() 
{

}
```

You can run it by clicking on 'Run'.

## 7.7 Exercise 4

 * Save the Processing program above in the folder of your repository
 * Upload it

\pagebreak

## 7.8 Modifying code

Navigate into the folder of your repository:

```
cd [repo_name]
```

For example,

```
cd iloverichel
```

Here, as usual, you can do `add`, `commit`, `push` and `pull`:

```
git add .
git commit -m "Improve"
git push
git pull
```

With Processing, you can edit the code.

\pagebreak

## 7.9 Exercise 3

Make something pretty together!

Each team member must 

 * add one line of code at a time
 * `add`, `commit` and `push` it
 * at least 5 commits per team member

Below is some example code:

```c++
void setup()
{
  size(256,256);  
}

void draw() 
{
  fill(mouseX, mouseY, mouseX + mouseY);
  ellipse(mouseX, mouseY, 50, 50);  
  fill(mouseY, mouseX, 255);
  ellipse(mouseY, mouseX, 50, 50);  
}
```

