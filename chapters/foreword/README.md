---
documentclass: extarticle
fontsize: 14pt
---

# Forword

This is a book about `git` and Codeberg for youngsters.
`git` is a version control manager.
Codeberg ([https://codeberg.org](https://codeberg.org)) is website
that allows one to use `git` with multiple people.
This book teaches how to use `git` and Codeberg

## About this book

This book is licensed with CC-BY-NC-SA.

![License of this book](CC-BY-NC-SA.png)

(C) Richèl Bilderbeek and all his students

With this booklet, you can do what you want,
as long as you refer to the original website
[https://codeberg.org/richelbilderbeek/git_for_youngsters]https://codeberg.org/richelbilderbeek/git_for_youngsters).
This booklet will always be free, both as in beer and freedom.

This book is a bit sloppy.
There are typosd and th*e layout a*lso i`s not al`w**ays pretty**.
Because this book is hosted on Codeberg, 
everyone can help at making this book a bit less sloppy.
