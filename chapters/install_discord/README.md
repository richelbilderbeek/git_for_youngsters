# 3. Install Discord

In this chapter, we install Discord

![](emoji_bowtie.png) | With Discord, we can talk to one another online
:-------------:|:----------------------------------------: 

\pagebreak

## 3.1 Exercise 1

Go to the Discord homepage at 
[https://discord.com/download](https://discord.com/download)

![](discord_homepage_with_border.png)

> The Discord homepage

![](emoji_bowtie.png) | Discord is used mostly by games and Twitch streamers
:-------------:|:----------------------------------------: 

\pagebreak

## 3.2 Exercise 2

At the Discord homepage, click in 'Install' to install Discord.

![](emoji_smiley.png) | Installing Discord is easy
:-------------:|:----------------------------------------: 

\pagebreak

## 3.3 Exercise 3

Start Discord. You will see the Discord dashboard:

![](discord_dashboard_with_border.png)

> The Discord dashboard

![](emoji_sunglasses.png) | After a while, you have many cool Discord servers here
:-------------:|:----------------------------------------: 

![](emoji_bowtie.png) | Discord is big, take your time to look around
:-------------:|:----------------------------------------: 

\pagebreak

## 3.4 Exercise 4

Time to add a Discord friend.

Click on 'Add Friend' and fill in a username from someone you
know, for example `richelbilderbeek#9002`. 

![](emoji_smiley.png) | Hopefully, you have picked a good Discord username yourself
:-------------:|:----------------------------------------: 

\pagebreak

## 3.5 Exercise 5

Time to go to our Discord server.

Ask your Discord friend to invite you to the Discord server.

You will get a PM with an Invite link. Click that link.

![](emoji_bowtie.png) | One can also use a direct Invite link to the server
:-------------:|:----------------------------------------: 

\pagebreak

## 3.6 Final exercise

On our Discord server, say something at 'The Big Table' channel.

![](discord_server_with_border.png)

> Our Discord server

![](emoji_bowtie.png) | A Discord server can do many things, take your time to look around
:-------------:|:----------------------------------------: 

![](emoji_sunglasses.png) | Of course, I will say the coolest thing!
:-------------:|:----------------------------------------: 


