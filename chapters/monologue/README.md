# 5. Monologue

![](emoji_bowtie.png) | A monologue is when you talk to someone without letting them talk back
:-------------:|:----------------------------------------: 

![](emoji_sunglasses.png) | Monologues are my favorite!
:-------------:|:----------------------------------------: 

In this chapter, we are going to use the basic `git` workflow
by writing a monologue.

\pagebreak

## 5.1 Have a Codeberg repository

![](your_new_repo.png)

In this chapter, we will use an example user, which is `richelbilderbeek` 
and and example repository name, which is `iloverichel`.

![](emoji_bowtie.png) | A repository is a website that works with `git`
:-------------:|:----------------------------------------: 

## 5.2 Exercise 1

Create a Codeberg page for your repository.

![](emoji_smiley.png) | 'repository' is often shortened to 'repo'
:-------------:|:----------------------------------------: 

\pagebreak

## 5.3 Cloning a repository

To clone your repository,
in Git Bash, or a Linux/Mac terminal, do:

```
git clone https://codeberg.org/richelbilderbeek/iloverichel.git
```

![](clone_repo.png)

This will download your online repository to a folder on your harddrive:

![](cloned_repo.png)

## 5.4 Exercise 2

Clone your repository.

![](emoji_sunglasses.png) | Star Wars 'The Clone Wars' was not about `git`
:-------------:|:----------------------------------------: 

\pagebreak

## 5.5 Navigate into the folder

After cloning a repo, we need to navigate into the folder 
where the repository is. Use `cd` and the repository name (in
this example `iloverichel`):

```
cd iloverichel
```

![](cd_repo_folder.png)

![](emoji_bowtie.png) | `cd` means 'Change directory'
:-------------:|:----------------------------------------: 

To make sure you are in that folder, use `ls`:

```
ls
```

![](ls_in_repo_folder.png)

![](emoji_bowtie.png) | `ls` means 'List'
:-------------:|:----------------------------------------: 

One file you will see is called `README.md`.

## 5.6 Exercise 3

Navigate into the folder where your repo is cloned.

\pagebreak

![](emoji_bowtie.png) | Use `cd` to change the directory/folder
:-------------:|:----------------------------------------: 

## 5.7 Use `git status`

`git status` is very helpful.
You can always use `git status` in a repo folder:

```
git status
```

`git status` gives many hints how to use `git`.

## 5.8 Exercise 4

Run `git status` in a repo folder. 
If you get an error message, you did not navigate into a repo folder.

![](emoji_sunglasses.png) | My `git status` is `Cool`
:-------------:|:----------------------------------------: 

\pagebreak

## 5.9 Edit `README.md`

`README.md` contains the front page of your GitHub repo.

![](emoji_bowtie.png) | `README.md` is in screamcase, due to history
:-------------:|:----------------------------------------: 

![](emoji_smiley.png) | `.md` is short for 'Markdown', which is a markup language 
:-------------:|:----------------------------------------: 

![](emoji_sunglasses.png) | HTML is a famous markup language
:-------------:|:----------------------------------------: 

To edit `README.md`, you can open it:

 * using a file browser (e.g. Microsoft File Manager, Apple Files, or Nautulus)
 * using Git Bash or a terminal

In Git Bash or the terminal, do:

```
[name of your editor] README.md
```

where `[name of your editor]` is the name of your editor, for example
`notepad`, `mousepad`, `edit`, `gedit`, `vim`, `emacs`. 

![](emoji_smiley.png) | There are many text editors!
:-------------:|:----------------------------------------: 

This is, for example:

```
notepad README.md
```

Now your editor will open up and you can modify the README.md file.

![](edit_readme.png)

The text will be similar to:

```
# iloverichel
```

Change the text in any way, as if having a monologue:

```
# iloverichel

Richel was here!
```

![](edited_readme.png)

Save the file and close the editor.

You will be taken back to Git Bash or terminal:

![](edited_readme_back_to_cli.png)

## 5.10 Exercise 5

Edit your `README.md`.

\pagebreak

## 5.11 `add`, `commit`, `push`

Now come the most commonly used three `git` commands.

![](emoji_smiley.png) | `git` can do a lot of things
:-------------:|:----------------------------------------: 

Tell `git` you want to 'stage' (whatever that is) all files you've changed:

```
git add .
```

Don't forget the dot (`.`) at the end!

Next, tell `git` what your changes were:

```
git commit -m "Improved monologue"
```

Between the `"`s you describe what you did.

![](emoji_bowtie.png) | Good programmers have commit messages that tells clearly what they did
:-------------:|:----------------------------------------: 

Finally, upload your work to Codeberg:

```
git push
```

If you did it correctly, your Codeberg repository will have your new text.

If you are asked for:

 * a password, type it in
 * another password, type it in again
 * an access token, see chapter 'Register and login' 

![](emoji_smiley.png) | `git` needs to be sure who you are
:-------------:|:----------------------------------------: 

After this, do another round of `add`, `commit`, `push` :-)

Congratulations, you just did the full workflow!

## 5.11 Exercise 6

![](repo_with_many_commits_annotated.png)

Add 5 commits by, for 5 times, adding a sentence and then `add`, `commit`, `push`.

Tip: with the arrow key upwards, you can get back previous commands!

![](emoji_sunglasses.png) | It is handy that Git Bash knows my previous commands!
:-------------:|:----------------------------------------: 


