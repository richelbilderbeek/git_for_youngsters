# Basic workflow

Here a list of useful commands:

```
git clone https://codeberg.org/richelbilderbeek/iloverichel
cd iloverichel
mousepad README.md
git add .
git commit -m "I did it"
git push
git pull
```

![](git_basic_workflow.png)
