# 4. Create a repository

In this lesson, we are going to create our own repository

![](emoji_bowtie.png) | A repository is a website that works with `git`
:-------------:|:----------------------------------------: 

\pagebreak

## 4.1. Exercise 1

Login to Codeberg. Then, on the dashboard, click 'New repository':

![](dashboard_new_repo_with_border.png)

This will take you to the 'New repository' screen.

![](emoji_smiley.png) | The dashboard shows all your repostories
:-------------:|:----------------------------------------: 

\pagebreak

## 4.2. Exercise 2

This is the 'New repository' screen:

![](new_repo_with_border.png)

Fill in the 'Repository name', which will be the -duh!- name of the repository.
In the case below, `iloverichel` is used.

![](emoji_smiley.png) | Maybe `richeliscrazy` is a better name
:-------------:|:----------------------------------------: 

![](new_repo_1_with_border.png)

Click on 'Initialize repository (Adds .gitignore, License and README)'.
This step is optional, but we want this :-) .

![](new_repo_2_with_border.png)

Now you have your new repo!

![](your_new_repo_with_border.png)

![](emoji_bowtie.png) | The website text shown, is from `README.md`
:-------------:|:----------------------------------------: 

\pagebreak

## 4.3. Final exercise

Create a new repository without looking at this booklet.

![](emoji_sunglasses.png) | This should be easy!
:-------------:|:----------------------------------------: 

